import os
import csv
import json
import pickle


# klasa bazowa do obsługi plików
class FileChanger:

    def __init__(self, line_args):
        self.line_args = line_args
        self.start_error()
        self.src_file = self.line_args[0]
        self.dst_file = self.line_args[1]
        self.changes = self.line_args[2:]

    def start_error(self):
        if len(self.line_args) < 2:
            print('''
                Niepoprawna ilość argumentów.
                Podaj: plik_zrodlowy plik_docelowy planowane_zmiany
                planowane zmiany w formacie:
                linia_do_zmiany,kolumna_do_zmiany,wartość_do_zmiany''')
            exit

    def file_exist(self):
        filename = self.src_file
        if os.path.isfile(filename):
            print(f'\nPlik {filename} istnieje.\n')
            return True
        else:
            print(f'\nPlik {filename} nie istnieje. Wybierz plik z listy.\n')
            actual_path = 'ls -al ' + os.getcwd()
            return os.system(actual_path)

    def file_extension(self, file):        
        return file.split('.')[-1].lower()

    def decompose_to_list(self):
        if self.file_exist():
            ext = self.file_extension(self.src_file)
            match ext:
                case 'csv':
                    print('CSV')
                    return CsvChanger.read_file(self)
                case 'json':
                    print('JSON')
                    return JsonChanger.read_file(self)
                case 'pkl':
                    print('PICKLE')
                    return PickleChanger.read_file(self)
                case _:
                    print(f'Brak obsługi plików z rozszerzeniem .{ext}')
                    return False

    def run_change(self, list_of_data):
        for change in self.changes:
            row, col, to_change = change.split(',')
            row = int(row)
            col = int(col)
            try:
                list_of_data[row][col] = to_change
            except:
                print(f'W zmianie "{change}" indeks poza zasięgiem.')
        return list_of_data

    def save_data_to_file(self, data_to_file):
        ext = self.file_extension(self.dst_file)
        match ext:
            case 'csv':
                print('Zapisuję do pliku .csv')
                CsvChanger.save_file(self, data_to_file)
            case 'json':
                print('Zapisuję do pliku .json')
                JsonChanger.save_file(self, data_to_file)
            case 'pkl':
                print('Zapisuję do pliku .pkl (PICKLE)')
                PickleChanger.save_file(self, data_to_file)
            case _:
                print(f'Brak obsługi plików z rozszerzeniem .{ext}')
                return False

class CsvChanger(FileChanger):

    def __init__(self, line_args):
        super().__init__(line_args)

    def read_file(self):
        with open(self.src_file, newline='') as file:
            reader = csv.reader(file)
            data_in_list = list(reader)
        return data_in_list

    def save_file(self, data_to_file):
        with open(self.dst_file, 'w', newline='') as file:
            writer = csv.writer(file)            
            writer.writerows(data_to_file)


class JsonChanger(FileChanger):
    
    def __init__(self, line_args):
        super().__init__(line_args)
    
    def read_file(self):
        with open(self.src_file, 'r') as file:
            datas = json.load(file)
            data_in_list = []
            for key in datas:
                t1 = [key]
                t2 = datas[key]
                temp_list = t1
                for element in t2:
                    temp_list.append(element)
                data_in_list.append(temp_list)
            return data_in_list
    
    def save_file(self, data_to_file):
        temp_data = {}
        for data in data_to_file:
            temp_data[data[0]] = data[1:]
        with open(self.dst_file, 'w') as file:
            json.dump(temp_data, file)


class PickleChanger(FileChanger):
    
    def __init__(self, line_args):
        super().__init__(line_args)

    def read_file(self):
        with open(self.src_file, 'rb') as file:
            data_in_list = pickle.load(file)
            return data_in_list

    def save_file(self, data_to_file):
        with open(self.dst_file, 'wb') as file:
            pickle.dump(data_to_file, file)