# coding: utf8

'''
Napisz program "reader.py", który zmodyfikuje plik csv i wyświetli w terminalu jego zawartość, a następnie zapisze w wybranej lokalizacji.
Wykonanie programu:

reader.py <src> <dst> <change1> <change2> ...
src to ścieżka pliku csv. Jeśli plik nie istnieje bądź podana ścieżka nie jest plikiem, wyświetl błąd i wskaż pliki w tym samym katalogu.
dst to docelowa ścieżka, w której zapiszemy zmieniony plik csv.

change1 ... changeN to ciągi znaków w postaci "Y,X,wartosc". Y to wiersz do zmodyfikowania (liczony od 0), X to kolumna (liczona od 0). Komórka pod wskazanym adresem powinna zmienić wartość na "wartosc"

*******************************************************************
Modyfikacja z lekcji 17
*******************************************************************

W przypadku formatu pickle, json, pliki zapisywane są jako listy list - każdy rząd to lista ciągów znaków, rzędy są przechowywane w liście.

Napisz program "reader.py", który zmodyfikuje plik csv, json, lub pickle <src> i wyświetli w terminalu jego zawartość, a następnie zapisze w wybranej lokalizacji.

Wykonanie programu:

reader.py <src> <dst> <change1> <change2> ..
src to ścieżka pliku csv. Jeśli plik nie istnieje bądź podana ścieżka nie jest plikiem, wyświetl błąd i wskaż pliki w tym samym katalogu.
dst to docelowa ścieżka w której zapiszemy zmieniony plik csv.

change1 ... changeN to ciągi znaków w postaci "Y,X,wartosc" Y. to wiersz do zmodyfikowania (liczony od 0), X to kolumna (liczona od 0). Komórka pod wskazanym adresem powinna zmienić wartość na "wartosc".

Rodzaj pliku (src i dst) powinien być wykrywany na podstawie rozszerzenia:
*.csv - plik csv
*.json - plik json
*.pickle - plik pickle

'''

from sys import argv
from helpers import FileChanger, CsvChanger

change_file = FileChanger(argv[1:])

def main():
    list_of_data = change_file.decompose_to_list()
    print(list_of_data)
    modified_data = change_file.run_change(list_of_data)
    print(modified_data)
    change_file.save_data_to_file(modified_data)

if __name__ == "__main__":
    main()